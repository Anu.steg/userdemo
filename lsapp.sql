-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 29, 2019 at 04:45 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.14-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `assign`
--

CREATE TABLE `assign` (
  `aid` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `pstartdate` varchar(250) DEFAULT NULL,
  `penddate` varchar(250) DEFAULT NULL,
  `pschedule` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign`
--

INSERT INTO `assign` (`aid`, `uid`, `pid`, `pstartdate`, `penddate`, `pschedule`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '2019-03-20', '2019-04-24', '2019-03-20', '2019-03-16 08:40:20', '2019-03-16 08:40:20'),
(2, 11, 3, '2019-03-23', '2019-04-26', '2019-03-23', '2019-03-16 08:44:08', '2019-03-16 08:44:08'),
(3, 4, 1, '2019-03-27', '2019-04-24', '2019-03-27', '2019-03-16 08:44:42', '2019-03-16 08:44:42'),
(4, 10, 4, '2019-03-25', '2019-04-25', '2019-03-25', '2019-03-16 08:45:06', '2019-03-16 08:45:06'),
(5, 9, 8, '2019-03-22', '2019-04-30', '2019-03-22', '2019-03-16 08:45:29', '2019-03-16 08:45:29'),
(6, 5, 6, '2019-03-18', '2019-04-18', '2019-03-18', '2019-03-16 08:45:53', '2019-03-16 08:45:53'),
(7, 3, 5, '2019-03-26', '2019-04-24', '2019-03-26', '2019-03-16 08:46:21', '2019-03-16 08:46:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `pid` int(11) NOT NULL,
  `pname` varchar(150) DEFAULT NULL,
  `pdescription` text,
  `pdocument` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`pid`, `pname`, `pdescription`, `pdocument`, `created_at`, `updated_at`) VALUES
(1, 'Login System', 'Logging in is usually used to enter a specific page, which trespassers cannot see. Once the user is logged in, the login token may be used to track what actions the user has taken while connected to the site.', 'loginsystem_1552712260.pdf', '2019-03-15 23:27:40', '2019-03-15 23:27:40'),
(2, 'MangoPay System', 'MangoPay is a service for market places, collaborative consumption platforms, and crowdfunding websites. It allows them to have a payment solution and to manage the electronic funds, with as particularity the management of e-wallet in white label.', 'MANGOPAY_1552712534.pdf', '2019-03-15 23:32:14', '2019-03-15 23:32:14'),
(3, 'Human Resource Management System', 'An HRMS (Human Resource Management System) is a combination of systems and processes that connect human resource management and information technology through HR software.', 'HRMS_1552712617.pdf', '2019-03-15 23:33:37', '2019-03-15 23:33:37'),
(4, 'Clash of Clans', 'Clash of Clans is a freemium mobile strategy video game developed and published by Finnish game developer Supercell. The game was released for iOS platforms on August 2, 2012, and on Google Play for Android on October 7, 2013.', 'Clash of Clans_1552712652.pdf', '2019-03-15 23:34:12', '2019-03-15 23:34:12'),
(5, 'Student Login System', 'A student information system (SIS), student management system, school administration software or student administration system is a management information system for education establishments to manage student data.', 'StudenLoginSystem_1552712796.pdf', '2019-03-15 23:36:36', '2019-03-15 23:36:36'),
(6, 'American Red Cross', 'The American Red Cross, also known as The American National Red Cross, is a humanitarian organization that provides emergency assistance, disaster relief, and disaster preparedness education in the United States.', 'AmericanRedCross_1552712859.pdf', '2019-03-15 23:37:39', '2019-03-15 23:37:39'),
(7, 'Employees Management System', 'An Employee management system is designed to simplify the process of record maintenance of employees in an organization. It helps in managing the information of employees for HR functions. In general, an employee management system is a part of a comprehensive Human Resource Management System.', 'EmployeeManagmentSystem_1552712931.pdf', '2019-03-15 23:38:51', '2019-03-15 23:38:51'),
(8, 'Naruto Anime Series', 'Naruto is a Japanese manga series written and illustrated by Masashi Kishimoto. It tells the story of Naruto Uzumaki, an adolescent ninja who searches for recognition from his peers and the village and also dreams of becoming the Hokage, the leader of his village.', 'download_1552712987.jpeg', '2019-03-15 23:39:47', '2019-03-15 23:39:47'),
(9, 'Game of Thrones', 'Nine noble families wage war against each other in order to gain control over the mythical land of Westeros. Meanwhile, a force is rising after millenniums and threatens the existence of living men.', 'Game of Thrones_1552713107.', '2019-03-15 23:41:47', '2019-03-15 23:41:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$qNWJr8nZpKoJdsSRVWqUeOXpEGhUMNqSWzBGEFq6ja5BDNCO7/HcC', 1, '35dBxtAQmuee8ZM9T1KKuqhBjdPCPhYXurDtQzYy116Yzq8Qg1y0BHDSpxOn', '2019-03-12 04:57:12', '2019-03-12 06:59:56'),
(2, 'Anurag', 'anu@gmail.com', NULL, '$2y$10$zo9W0iMOXhwPTp3ALt1.turRgz2bpagguWa0yEUFVpAmTRcTDXIg6', 2, 'rD6BJfcGcVYKGQZHCCHvKXsWz312TrHUYEKYcgmFlvyO7dS8LGG9w0rdtiQ4', '2019-03-12 03:56:58', '2019-03-12 06:57:41'),
(3, 'Test', 'test@gmail.com', NULL, '$2y$10$OVoOVNR/NJqDiFoQonODu.yLPhe47Dwz1ztBDt4cQ8tWXP5iINXGu', 2, 'ZeJN8UhReTFcCwbldwc0BbLUF1Gg74j7Ex70ys1eHQCPDVa9bnIKocodYEb2', '2019-03-12 04:13:03', '2019-03-12 04:13:03'),
(4, 'Vaiku', 'veku@gmail.com', NULL, '$2y$10$nG39PhDroXIesHMk46Qguu9w0rBSZNJSZZwpKRew5o737WMxMjLLC', 2, NULL, '2019-03-12 04:22:11', '2019-03-12 04:22:11'),
(5, 'Siddharath', 'sidd@gmail.com', NULL, '$2y$10$hHFELkClGIN3M2rN1/zheeyi/ZJR.njhp/T6jvZY77XR3Z6JhaNze', 2, NULL, '2019-03-12 04:25:56', '2019-03-12 06:53:52'),
(6, 'Demo', 'demo@gmail.com', NULL, '$2y$10$Yw0.CzxtiCt8BkvTD3VRnuKp99Z/yhQvvGWwlTkktCabWNcYVpI4O', 2, NULL, '2019-03-12 04:30:02', '2019-03-12 04:30:02'),
(7, 'Rohini', 'rohini@gmail.com', NULL, '$2y$10$F0udSKX8fMKLes0jkk33be9p98HJ4rnqPJfayq6JObD4hYB87CTra', 2, NULL, '2019-03-12 04:43:52', '2019-03-12 04:43:52'),
(8, 'Janak', 'janak@gmail.com', NULL, '$2y$10$aSKHvQhGrJfvcRHFB6OPMeZY4fz0ScDPUU1VL//DYuZQXJJ4jJ66e', 2, NULL, '2019-03-12 04:51:05', '2019-03-12 04:51:05'),
(9, 'Vishal', 'vishal@gmail.com', NULL, '$2y$10$da/XMHf3osPJDJTHy58fl.cMeaQ9pamnBGqa1ZGaZtMdUUADupiga', 2, NULL, '2019-03-12 04:53:51', '2019-03-12 04:53:51'),
(10, 'Chirag', 'chirag@gmail.com', NULL, '$2y$10$M5FW3q9yZMM0lVw3ZqPCBOZP6e3WMIvWHGsQHZsu.Zn9Lpja1ME7O', 2, NULL, '2019-03-12 05:02:47', '2019-03-12 05:02:47'),
(11, 'Parth', 'parth@gmail.com', NULL, '$2y$10$kD5icLUPXUyueVzlj.CZ8eGrkzh7SBWUirUc6Sw0v3Ees2UtAEOKW', 2, NULL, '2019-03-12 05:05:35', '2019-03-12 05:05:35'),
(12, 'Diya', 'diya@gmail.com', NULL, '$2y$10$bUlxRzcbHO4c4kLGaAL3/.fjFN6UTu8CRfM/yuNbMFofy.6phfoZO', 2, NULL, '2019-03-12 05:16:11', '2019-03-12 05:16:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assign`
--
ALTER TABLE `assign`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assign`
--
ALTER TABLE `assign`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
