<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Project;
use App\Assign;
use File;


class PageController extends Controller
{
    public function index() {

      return view('dashboard');
    }

    public function userlist() {
      $users =  User::where('id', '!=', Auth::user()->id)->get();
      return view('users.userlist')->with('shows', $users);
    }

    public function userdash()
    {
      return view('userdashboard');
    }

    public function projectdash() {
      $data = Project::get();
      return view('project.project')->with('data',$data);
    }

    public function userdetail() {
      $data = Assign::where('aid','==',Auth::user()->id)->get();
      return view('users.userdetail')->with('data', $data);
    }

    public function assign() {
      $data['user'] = User::where('role','!=',1)->orderBy('id','desc')->get();
      $data['project'] = Project::orderBy('pid','desc')->get();
      return view('project.assign')->with($data);
    }

}
