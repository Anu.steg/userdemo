<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validater;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

  public function create() {

    return view('users.create');

  }

  public function adduser(Request $request) {

      $this->validate($request,[

        'name' => 'required',
        'email' => 'required',
        'password' => 'required'
      ]);

      $store = new User;
      $store->name = $request->name;
      $store->email = $request->email;
      $store->password = Hash::make($request->password);
      $store->save();

      return redirect()->route('userlist.index')->with('success', 'User added successfully ');

  }

  public function showuser($id) {

    $user = User::find($id);
    return view('users.edit')->with('user', $user);

  }

  public function edituser(Request $request,$id) {

    $this->validate($request,[

      'name' => 'required',
      'email' => 'required'
    ]);

    $update = User::find($id);
    $update->name = $request->name;
    $update->email = $request->email;
    $update->save();

    return redirect()->route('userlist.index')->with('success', 'User update successfully ');

  }

  public function delete($id) {

    $user = User::find($id);
    $user->delete();

    return redirect()->route('userlist.index')->with('success', 'User delete successfully');

  }

}
