@extends('layouts.app')
@section('content')
      <h1>Project List</h1>

      <div class="col-sm-12 mb-1">
        <a class="btn btn-info float-right"  href="{{route('add.index')}}"><i class="fa fa-paper-plane"></i> Add          </a>
      </div>

      <div class="table-responsive">
          <table class="table">
              <thead class="bg-success white text-center">
                  <tr>
                      <th>Project Name</th>
                      <th>Project Description</th>
                      <th>Project Document</th>
                      <th colspan="2">Action</th>
                  </tr>
              </thead>
              <tbody class="text-center">
                @foreach ($data as $row)
                  <tr>
                      <td>{{$row->pname}}</td>
                      <td>{{$row->pdescription}}</td>
                      <td class="text-center"> <a href="storage/upload/{{$row->pdocument}}"> <i class="fa fa-eye btn btn-primary"></i> </a></td>
                      <td>
                        <a href="{{route('edit.index',[$row->pid])}}"> <i class="fa fa-pencil-square-o btn btn-info"></i> </a>
                        <a href="{{route('delete.index',[$row->pid])}}"> <i class="fa fa-trash-o btn btn-danger"></i> </a>
                      </td>
                  </tr>
                @endforeach
              </tbody>
          </table>

      </div>


@endsection
