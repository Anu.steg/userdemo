<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
   <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/dashboard-analytics.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Oct 2018 09:17:48 GMT -->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
      <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
      <meta name="author" content="PIXINVENT">
      <title>Lsapp</title>
      <link rel="apple-touch-icon" href="{{asset('assets/images/ico/apple-icon-120.png')}}">
      <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/bootstrap-admin-template/robust/app-assets/images/ico/favicon.ico">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,400,500,700" rel="stylesheet">
      <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <!-- BEGIN VENDOR CSS-->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/jquery-jvectormap-2.0.3.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/morris.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/unslider.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/weather-icons/climacons.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/toastr.css')}}">
      <script src="{{asset('assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/js/scripts/extensions/toastr.js')}}" type="text/javascript"></script>
      <!-- END VENDOR CSS-->
      <!-- BEGIN ROBUST CSS-->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/app.css')}}">
      <!-- END ROBUST CSS-->
      <!-- BEGIN Page Level CSS-->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/menu/menu-types/vertical-menu.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/colors/palette-gradient.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/calendars/clndr.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/extensions/toastr.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/core/colors/palette-climacon.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/users.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/meteocons/style.min.css')}}">
      <!-- END Page Level CSS-->
      <!-- BEGIN Custom CSS-->
      <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
      <!-- END Custom CSS-->
   </head>
   <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
      <!-- fixed-top-->
      <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
         <div class="navbar-wrapper">
            <div class="navbar-header">
               <ul class="nav navbar-nav flex-row">
                  <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                  <li class="nav-item">
                     <a class="navbar-brand" href="{{route('dashboard.index')}}">
                        <img class="brand-logo" alt="robust admin logo" src="{{asset('assets/images/logo/logo-light-sm.png')}}">
                        <h3 class="brand-text">Robust Admin</h3>
                     </a>
                  </li>
                  <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
               </ul>
            </div>
            <div class="navbar-container content">
               <div class="collapse navbar-collapse" id="navbar-mobile">
                  <ul class="nav navbar-nav mr-auto float-left">
                     <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu">         </i></a></li>
                  </ul>
                  <ul class="nav navbar-nav float-right">
                     <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar avatar-online"><img src="{{asset('assets/images/portrait/small/avatar-s-1.png')}}" alt="avatar"><i></i></span><span class="user-name">{{Auth::User()->name}}</span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                           <a class="dropdown-item" href="#"><i class="ft-user"></i> Edit Profile</a>
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="{{route('logout')}}"><i class="ft-power"></i> Logout</a>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </nav>
      <!-- ////////////////////////////////////////////////////////////////////////////-->
      <div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
         <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
              @if (Auth::user()->role == 1)
                <li class=" nav-item"><a href="{{route('dashboard.index')}}"><i class="icon-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a></li>
                <li class=" nav-item"><a href="{{route('project.index')}}"><i class="icon-folder"></i><span class="menu-title" data-i18n="nav.dash.main">Project</span></a></li>
                <li class=" nav-item"><a href="{{route('assign.index')}}"><i class="icon-briefcase"></i><span class="menu-title" data-i18n="nav.dash.main">Assign Project</span></a></li>
                <li class="nav-item">
                  <a href="{{route('userlist.index')}}"><i class="fa fa-user-circle-o"></i><span class="menu-title" data-i18n="nav.dash.main"> User List</span></a>
                </li>
              @else
                <li class=" nav-item">
                  <a href="{{route('userdashboard.index')}}"><i class="icon-home"></i><span class="menu-title" data-i18n="nav.dash.main"> User Dashboard</span></a>
                </li>
                <li class=" nav-item">
                  <a href="{{route('userdetail.index')}}"><i class="icon-home"></i><span class="menu-title" data-i18n="nav.dash.main"> User Detail</span></a>
                </li>
              @endif
            </ul>
         </div>
      </div>
      <div class="app-content content">
         <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
              @include('inc.message')
               @yield('content')
            </div>
         </div>
      </div>

      <footer class="footer footer-static footer-light navbar-border">
         <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span></p>
      </footer>
      <!-- BEGIN VENDOR JS-->
      <script src="{{asset('assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
      <!-- BEGIN VENDOR JS-->
      <!-- BEGIN PAGE VENDOR JS-->
      <script src="{{asset('assets/vendors/js/extensions/jquery.knob.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/js/scripts/extensions/knob.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/data/jvector/visitor-data.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/jquery.sparkline.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/extensions/unslider-min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/extensions/moment.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/extensions/underscore-min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/extensions/clndr.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/charts/echarts/echarts.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/vendors/js/extensions/unslider-min.js')}}" type="text/javascript"></script>

      <!-- END PAGE VENDOR JS-->
      <!-- BEGIN ROBUST JS-->
      <script src="{{asset('assets/js/core/app-menu.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/js/core/app.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/js/scripts/customizer.js')}}" type="text/javascript"></script>

      <!-- END ROBUST JS-->
      <!-- BEGIN PAGE LEVEL JS-->
      <script src="{{asset('assets/js/scripts/pages/dashboard-analytics.js')}}" type="text/javascript"></script>
      <script src="{{asset('assets/js/scripts/pages/dashboard-ecommerce.js')}}" type="text/javascript"></script>
      <!-- END PAGE LEVEL JS-->
   </body>
   <!-- Mirrored from pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/dashboard-analytics.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Oct 2018 09:19:34 GMT -->
</html>
