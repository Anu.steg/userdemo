@extends('layouts.app')
@section('content')

  <h2>Shows Users</h2>
  <div class="table-responsive">
      <table class="table">
          <thead class="bg-success white">
              <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($shows as $show)
              <tr>
                <td>{{$show->name}}</td>
                <td>{{$show->email}}</td>
                <td>
                  <a href="{{route('user.edit',[$show->id])}}"> <i class="fa fa-pencil-square-o btn btn-info"></i> </a>
                  <a href="{{route('user.delete',[$show->id])}}"> <i class="fa fa-trash-o btn btn-danger"></i> </a>
                </td>
              </tr>
            @endforeach
          </tbody>
      </table>
      <div class="col-sm-12 mb-1">
        <a class="btn btn-info" href="{{route('user.create')}}"><i class="fa fa-paper-plane"></i> Create          </a>
      </div>
  </div>
@endsection
