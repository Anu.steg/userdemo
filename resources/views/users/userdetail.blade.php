@extends('layouts.app')
@section('content')

  <div class="row">
    <div class="col-12">
      <div class="card profile-with-cover">
        <div class="card-img-top img-fluid bg-cover height-300" style="background: url('{{asset('assets/images/carousel/22.jpg')}}') 50%;"></div>
        <div class="media profil-cover-details w-100">
          <div class="media-left pl-2 pt-2">
            <a href="#" class="profile-image">
              <img src="{{asset('assets/images/portrait/small/avatar-s-1.png')}}" class="rounded-circle img-border height-100" alt="Card image">
            </a>
          </div>
          <div class="media-body pt-3 px-2">
            <div class="row">
              <div class="col">
                <h3 class="card-title">{{Auth::User()->name}}</h3>
              </div>
            </div>
          </div>
        </div>

        <nav class="navbar navbar-light navbar-profile align-self-end">
          <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation"></button>
        </nav>
      </div>
    </div>
  </div>


  <section id="timeline" class="timeline-center timeline-wrapper">
    <h3 class="page-title text-center">Timeline</h3>

    <ul class="timeline">
      <li class="timeline-item">
        <div class="timeline-card card border-grey border-lighten-2">
          <div class="card-header">
            <h4 class="card-title"><a href="#">Profile Detail</a></h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="card-content">
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-content collapse show">
                    <div class="card-body">
                      <form class="form-horizontal" novalidate>
                        <div class="row">
                          <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                              <h5>Name </h5>
                              <div class="controls">
                                <input type="text" name="text" class="form-control" required data-validation-required-message="This field is required" value="{{Auth::user()->name}}">
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Email </h5>
                              <div class="controls">
                                <input type="email" name="email" class="form-control" required data-validation-required-message="This field is required" value="{{Auth::user()->email}}">
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Project Name </h5>
                              <div class="controls">
                                <input type="text" name="text" class="form-control" required data-validation-required-message="This field is required" value="">
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Project Description </h5>
                              <div class="controls">
                                <textarea name="text" rows="3" cols="60"></textarea>
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Project Document </h5>
                              <div class="controls">
                                <input name="text" class="form-control" required data-validation-required-message="This field is required" value="">
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Project Start Date</h5>
                              <div class="controls">
                                <input type="date" class="form-control" name="pstd" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Date Opened" value="">
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Project End Date</h5>
                              <div class="controls">
                                <input type="date" class="form-control" name="pend" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Date Fixed">
                              </div>
                            </div>
                            <div class="form-group">
                              <h5>Project Schedule Date</h5>
                              <div class="controls">
                                <input type="date" class="form-control" name="pscd" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Date Fixed">
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </section>

@endsection
