@if (session('error'))
  {{-- <div class="alert alert-danger alert-dismissible mb-2" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="type-danger"><span aria-hidden="true">&times;</span></button>
    <strong>{{session('error')}}</strong>
  </div> --}}
  <script>
    toastr.warning("{{session('error')}}");
  </script>
@endif

@if (session('success'))
    {{-- <button class="dismiss" data-dismiss="alert" id="type-success"></button>
    <strong>{{session('success')}}</strong> --}}
    <script>
      toastr.success("{{session('success')}}");
    </script>
@endif

@if(count($errors)>0)
  @foreach ($errors as $error)
    {{-- <div class="alert alert-danger alert-dismissible mb-2" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="type-danger"><span aria-hidden="true">&times;</span></button>
      <strong>{{session($error)}}</strong>
    </div> --}}
    <script>
      toastr.warning("{{session($error)}}");
    </script>
  @endforeach
@endif
