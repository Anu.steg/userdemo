<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('loginpage');
// });
Route::get('/logout','LoginController@logout')->name('logout');

Route::prefix('login')->group(function(){
  Route::get('/','LoginController@index')->name('login.index');
  Route::post('/checklogin','LoginController@checklogin')->name('check.login');
});

Route::prefix('admin')->group(function(){
  Route::get('/dashboard', 'PageController@index')->name('dashboard.index');
  Route::get('/userlist', 'PageController@userlist')->name('userlist.index');
  Route::get('/create', 'UserController@create')->name('user.create');
  Route::post('/save', 'UserController@adduser')->name('user.save');
  Route::get('/showuser/{id}', 'UserController@showuser')->name('user.edit');
  Route::post('/update/{id}', 'UserController@edituser')->name('user.update');
  Route::get('/delete/{id}','UserController@delete')->name('user.delete');
  Route::get('/assign', 'PageController@assign')->name('assign.index');
  // Route::get('/delete', 'PageController@delete')->name('admindashboard.delete');
});

Route::prefix('user')->group(function(){
  Route::get('/dashboard', 'PageController@userdash')->name('userdashboard.index');
  Route::get('/userdetail', 'PageController@userdetail')->name('userdetail.index');
  // Route::post('/save', 'UserController@adduser')->name('user.save');
  // Route::get('/showuser/{id}', 'UserController@showuser')->name('user.edit');
  // Route::post('/update/{id}', 'UserController@edituser')->name('user.update');
  // Route::get('/delete/{id}','UserController@delete')->name('user.delete');
  // Route::get('/update', 'PageController@update')->name('admindashboard.update');
  // Route::get('/delete', 'PageController@delete')->name('admindashboard.delete');
});


Route::prefix('project')->group(function(){
  Route::get('/', 'PageController@projectdash')->name('project.index');
  Route::get('/addproject', 'ProjectController@add')->name('add.index');
  Route::post('/store', 'ProjectController@store')->name('store.index');
  Route::get('/edit/{pid}', 'ProjectController@edit')->name('edit.index');
  Route::post('/update/{pid}', 'ProjectController@update')->name('update.index');
  Route::get('/delete/{pid}', 'ProjectController@delete')->name('delete.index');
  Route::post('/assignproject','ProjectController@assignproject')->name('assignpro.index');
  Route::get('/showname/{id}', 'PageController@update')->name('admindashboard.update');
  // Route::get('/delete', 'PageController@delete')->name('admindashboard.delete');
});
